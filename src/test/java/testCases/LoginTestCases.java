package testCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginTestCases {
    WebDriver driver;
    HomePage homePage;
    LoginPage loginPage;
    String expectedLoginFieldError = "Please enter your Spotify username or email address.";
    String expectedPasswordFieldError = "Please enter your password.";
    String expectedBannerError = "Incorrect username or password.";


    @BeforeTest
    public void profileSetUp(){System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");}

    @BeforeMethod
    public void testSetUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://open.spotify.com/");
    }

    @Test
    public void testCase1(){
        homePage = new HomePage(driver);
        homePage.clickLoginButton();
        loginPage = new LoginPage(driver);
        loginPage.enterLoginValue("a");
        loginPage.enterPasswordValue("b");
        loginPage.clearLoginField();
        loginPage.clearPasswordField();
        assertTrue(loginPage.getTextLoginFieldError().equals(expectedLoginFieldError) && loginPage.getTextPasswordFieldError().equals(expectedPasswordFieldError));
    }

    @Test
    public void testCase2(){
        homePage = new HomePage(driver);
        homePage.clickLoginButton();
        loginPage = new LoginPage(driver);
        loginPage.enterLoginValue("abc");
        loginPage.enterPasswordValue("bcd");
        loginPage.clickSubmitButton();
        assertTrue(loginPage.getTextBannerError().equals(expectedBannerError));
    }

    @Test
    public void testCase3(){
        homePage = new HomePage(driver);
        homePage.clickLoginButton();
        loginPage = new LoginPage(driver);
        loginPage.enterLoginValue("ss1979@ukr.net");
        loginPage.enterPasswordValue(",aU,p_s*pgTz97K");
        loginPage.clickSubmitButton();
        assertEquals(homePage.getTextUserName(), "Serhii");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
