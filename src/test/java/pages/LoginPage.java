package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class LoginPage {
    WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    By loginField = By.id("login-username");
    By passwordField = By.id("login-password");
    By submitButton = By.id("login-button");
    By loginFieldError = By.xpath("//div[@id='username-error']/span/p");
    By passwordFieldError = By.xpath("//div[@id='password-error']/span");
    By bannerError = By.xpath("//div[@data-encore-id='banner']/span");

    public void enterLoginValue(String loginKeys){
        waitVisibilityOfElement(loginField);
        driver.findElement(loginField).sendKeys(loginKeys);
        //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }
    public void enterPasswordValue(String passwordKeys){
        waitVisibilityOfElement(passwordField);
        driver.findElement(passwordField).sendKeys(passwordKeys);
        //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    public void clearLoginField(){
        driver.findElement(loginField).sendKeys(Keys.BACK_SPACE);
        //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    public void clearPasswordField(){
        driver.findElement(passwordField).sendKeys(Keys.BACK_SPACE);
        //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    public void clickSubmitButton(){
        driver.findElement(submitButton).click();
    }

    public String getTextLoginFieldError(){
        waitVisibilityOfElement(loginFieldError);
        return driver.findElement(loginFieldError).getText();
    }

    public String getTextPasswordFieldError(){
        waitVisibilityOfElement(passwordFieldError);
        return driver.findElement(passwordFieldError).getText();
    }

    public String getTextBannerError(){
        waitVisibilityOfElement(bannerError);
        return driver.findElement(bannerError).getText();
    }

    public void waitVisibilityOfElement(By by) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }
}
